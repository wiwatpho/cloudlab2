# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt
import os, uuid
import azure.functions as func
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient
from typing import List, Tuple

def main(file: str) -> List[Tuple[int, str]]:
    # Get the blob storage connection string from Durable Functions environment variables

    connect_str = os.environ['AZURE_STORAGE_CONNECTION_STRING']
    #for tes locally
    #connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
    container_name = 'azuredurablefunctionblobstore'

    # define file name prefix
    print(file)
    blob_prefix = file
    #blob_prefix = 'mrinput-'

    # Create a BlobServiceClient
    blob_service_client = BlobServiceClient.from_connection_string(connect_str)

    # Get a reference to the container
    container_client = blob_service_client.get_container_client(container_name)

    # Get the list of blobs in the container
    blob_list = container_client.list_blobs(name_starts_with=blob_prefix)

    print(blob_list)
    # Initialize the result list
    result = []

    # Iterate through each blob
    for blob in blob_list:
        # Get a reference to the blob
        blob_client = container_client.get_blob_client(blob.name)

        # Download the blob content
        blob_content = blob_client.download_blob().readall()

        # Split the content into lines
        lines = blob_content.decode('utf-8').splitlines()

        # Build the result list with offset and line string
        for offset, line in enumerate(lines):
            result.append((offset, line))
    print("get file") 
    print(result) 
    return result
