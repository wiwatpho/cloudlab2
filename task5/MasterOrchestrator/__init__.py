# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df
from typing import List


import logging
from typing import List
import azure.durable_functions as df

def orchestrator_function(context: df.DurableOrchestrationContext):
   # input_data = ["Up through the atmosphere", "Up where the air is clear", "Up Up Up high air"]
    
    # Get a list of input files
    files = [f'mrinput-{i}.txt' for i in range(1, 5)]

    # Get input data (Fan-Out)
    input_tasks = [context.call_activity('GetInputDataActivity', file) for file in files]
    # Wait for all map geninputtask to complete (Fan-In)
    input_data_list = yield context.task_all(input_tasks)

    #Map phase(Fan-Out)
    map_tasks = [context.call_activity('MapperActivity', line) for input_data in input_data_list for line in input_data]
    # Wait for all map tasks to complete (Fan-In)
    map_results_list = yield context.task_all(map_tasks)

    # Flatten the list of map results
    map_results = [item for sublist in map_results_list for item in sublist]


    # Shuffle phase
    shuffle_task = yield context.call_activity("ShufflerActivity", map_results)

    # Reduce phase(Fan-Out)
    reduce_tasks = [context.call_activity('ReducerActivity', key_values) for key_values in shuffle_task.items()]
    # Wait for all reduce tasks to complete (Fan-In)
    reduce_results_list = yield context.task_all(reduce_tasks)

 
    # Flatten the list of reduce results
    reduce_results = [item for sublist in reduce_results_list for item in sublist]

    print(reduce_results)
    return reduce_results


   # print(reduce_task)
   # return reduce_task

main = df.Orchestrator.create(orchestrator_function)