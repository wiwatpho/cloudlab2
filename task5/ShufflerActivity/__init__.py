# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from typing import List

def main( mapresults: List[dict]) -> dict:
    shuffled_results = {}
    for result in mapresults:
        key = result["key"]
        if key not in shuffled_results:
            shuffled_results[key] = []
        shuffled_results[key].append(result["value"])

    return shuffled_results