# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from typing import List,Tuple

def main(inputpair: List) -> List[dict]:
    print(inputpair)
    line = inputpair[1]
    words = line.split()
    map_results = [{"key": word.lower(), "value": 1} for word in words]
    return map_results


