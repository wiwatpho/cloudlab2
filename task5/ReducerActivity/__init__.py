# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from typing import List

def main(shuffledresults: dict) -> List[dict]:
    reduce_results = []
    #for key, values in shuffledresults.items():
    key, values = shuffledresults
    total_count = sum(values)
    reduce_results.append({"key": key, "value": total_count})

    return reduce_results
