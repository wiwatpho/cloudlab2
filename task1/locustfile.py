#need pip install locust
# run with command: cloudlab2 % locust -f locustfile.py --host=http://127.0.0.1:5000 --headless -u 100  --run-time 3m --csv local --html reportlocal.html
# azure function: locust -f locustfile.py --host=https://azurelab2-numinfunc.azurewebsites.net --headless -u 100  --run-time 3m --csv azurefunction --html reportfunction.html

import time
from locust import HttpUser, task , between

class QuickstartUser(HttpUser):
    @task
    def hello_world(self):
        self.client.get("/numericalintegralservice/0/3.14159")