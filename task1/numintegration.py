import numpy as np

# define abs function
def f(x):
    return abs(np.sin(x))

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    x_values = np.linspace(lower, upper, N+1)
    
    integral_sum = 0
    for i in range(N):
        integral_sum += f(x_values[i]) * delta_x
    
    return integral_sum

# Main loop
#define upper and lower interval
interval_lower = 0
interval_upper = 3.14159

#define 7 N values
N_values = [10, 100, 100, 1000, 10000, 100000, 1000000]

for N in N_values:
    result = numerical_integration(interval_lower, interval_upper, N)
    print(f"For N = {N}: {result}")
