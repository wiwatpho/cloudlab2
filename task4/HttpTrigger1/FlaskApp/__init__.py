# need pip3 install flask
from flask import Flask, jsonify
import numpy as np
import logging

app = Flask(__name__)

# Define abs function
def f(x):
    return abs(np.sin(x))

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    x_values = np.linspace(lower, upper, N+1)
    
    integral_sum = 0
    for i in range(N):
        integral_sum += f(x_values[i]) * delta_x
    
    return integral_sum


# define default route link to check which on is running
@app.route("/")
def helloworld():
  	return 'Hello World! Azure Function'


# define route link
@app.route('/numericalintegralservice/<lower>/<upper>', methods=['GET'])
def calculate_integral(lower, upper):
    
    #cast
    lower = float(lower)
    upper = float(upper)

    N_values = [10, 100, 100, 1000, 10000, 100000, 1000000]
    results = []

    for N in N_values:
        result = numerical_integration(lower, upper, N)
        results.append({"N": N, "result": result})

    return jsonify(results)


if __name__ == '__main__':
   # app.run(debug=True)
    app.run(port=5000)
